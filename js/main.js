/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing) {
    this._size = size;
    this._stuffing = stuffing;
    this.toppingArr = [];
    try {
        if (!size || !stuffing) {
            throw new HamburgerException("Не хватает параметров для создания бургера!!!");
        } 
        if (size == Hamburger.SIZE_SMALL || size == Hamburger.SIZE_LARGE) {
            this._size = size;
        } else {
            throw new HamburgerException("Введите корректный параметр размера бургера!!!");
        }
        if (stuffing == Hamburger.STUFFING_CHEESE || stuffing == Hamburger.STUFFING_SALAD || stuffing == Hamburger.STUFFING_POTATO) {
            this._stuffing = stuffing;
        }else {
            throw new HamburgerException("Введите корректный параметр начинки бургера!!!");
        }
    } catch(err) {
        console.log(err.name +" "+ err.message)
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {"size": "small", "price" : 50, "calories" : 20};
Hamburger.SIZE_LARGE = {"size": "large", "price" : 100, "calories" : 40};
Hamburger.STUFFING_CHEESE = {"name": "cheese", "price" : 10, "calories" : 20};
Hamburger.STUFFING_SALAD = {"name": "salad", "price" : 20, "calories" : 5};
Hamburger.STUFFING_POTATO = {"name": "potato", "price" : 15, "calories" : 10};
Hamburger.TOPPING_MAYO = {"name": "mayo", "price" : 20, "calories" : 5};
Hamburger.TOPPING_SPICE = {"name": "spice", "price" : 15, "calories" : 0};

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (topping) {
    try{
        if(!topping){
            throw new HamburgerException("Топпинг отсутствует!")
        }
        if(topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE){
            throw new HamburgerException("Не коректное значение топпинга!")
        }
        if(this.toppingArr.indexOf(topping) !== -1) {
            throw new HamburgerException("Такой топпинг уже есть!")
        }
        this.toppingArr.push(topping);
    }catch (err){
        console.log(err.name +" "+ err.message)
    }
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param deleteTopping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (deleteTopping) {
    var topping = this.toppingArr;
    try {
        if(!deleteTopping || topping.length < 1) {
            throw new HamburgerException("Не корректная попытка удаления топпинга!");
        }
        if(topping.includes(deleteTopping)) {
            topping.splice(topping.indexOf(deleteTopping), 1);
        } else {
            throw new HamburgerException("Такого топпинга нет в бургере!")
        }
    } catch(err) {
        console.log(err.name +" "+ err.message);
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    var toppingName = this.toppingArr.map(function(item){
        return item.name;
    })
    console.log(toppingName);
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    console.log(this._size.size);
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    console.log(this._stuffing.name)
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var burgerPrice = this.toppingArr.slice(0);
    burgerPrice.push(this._size);
    burgerPrice.push(this._stuffing);
    var priceArr = burgerPrice.map(function(item) {
        return item.price;
      });
      console.log(priceArr);
    var result = priceArr.reduce(function(sum, current) {
        return sum + current;
      });
      console.log(result +" тугриков");
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var burgerCaloies = this.toppingArr.slice(0);
    burgerCaloies.push(this._size);
    burgerCaloies.push(this._stuffing);
    console.log(burgerCaloies);
    var caloriesArr = burgerCaloies.map(function(item) {
        return item.calories;
      });
    var result = caloriesArr.reduce(function(sum, current) {
        return sum + current;
      });
      console.log(result +" калорий");
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */

function HamburgerException (message) {
    this.name = "HamburgerException";
    this.message = message;
} 
HamburgerException.prototype = Error.prototype;

var burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
burger.addTopping(Hamburger.TOPPING_SPICE);
burger.addTopping(Hamburger.TOPPING_MAYO);
burger.removeTopping(Hamburger.TOPPING_MAYO);
// burger.removeTopping(Hamburger.TOPPING_SPICE);
burger.getToppings();
burger.getSize();
burger.getStuffing();
burger.calculatePrice();
burger.calculateCalories()


